<footer>
<p>"The Great Hambino" is a small homage to one of my favorite baseball films, <a href="http://www.imdb.com/title/tt0108037/?ref_=nv_sr_1" target="_blank">The Sandlot</a>, and the team's legendary catcher, <a href="https://www.youtube.com/watch?v=4uc7beYpGXM" target="_blank">Hamilton 'The Great Hambino' Porter</a>.</p> 
</footer>
</div>
<?php wp_footer(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83016220-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
