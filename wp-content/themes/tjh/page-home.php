<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

<main class="home">
    
    <div class="work-wrap"><!--wrapper-->
    
                   
                   <?php if(get_field('grid_items')): ?>

	<?php while(has_sub_field('grid_items')): ?>

        <a href="<?php echo get_sub_field('project_link'); ?>" class="work-item panel--<?php echo get_sub_field('project_size'); ?>">
            <div class="work-item__image" style="background:#ddd url(<?php echo get_sub_field('project_thumb'); ?>) no-repeat center center; background-size: cover;"></div>
                  <div class="work-item__content">
            <h3><?php echo get_sub_field('project_name'); ?></h3>
            <h4><?php echo get_sub_field('project_type'); ?></h4>
      </div>
        </a>

	<?php endwhile; ?>

<?php endif; ?>
                   
  </div> <!--work-wrap-->

</main>

<?php get_footer(); ?>
