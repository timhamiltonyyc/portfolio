<?php get_header(); ?>

<main class="work">


<?php if(get_field('project_images')): ?>

	<?php while(has_sub_field('project_images')): ?>

        <img src="<?php echo get_sub_field('project_image'); ?>">

	<?php endwhile; ?>

<?php endif; ?>

</main>

<?php get_footer(); ?>