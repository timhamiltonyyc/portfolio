<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<script src="https://use.typekit.net/msf5nmy.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title>Tim Hamilton &ndash; Calgary, Alberta Web and Graphic Designer</title>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <?php $ogurl = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
    <meta property="og:image" content="<?php echo $ogurl; ?>">

<link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() ) ?>/images/favicon.png">
<link rel="apple-touch-icon" href="<?php echo esc_url( get_template_directory_uri() ) ?>/images/apple-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_url( get_template_directory_uri() ) ?>/images/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_url( get_template_directory_uri() ) ?>/images/apple-icon-114x114.png">

<?php wp_head(); ?> 
</head>

<body <?php body_class(); ?>>
<div class="wrapper">
<header>
 
 <?php if ( is_front_page() ) {
    get_template_part('home_header');
 } 
else {
    get_template_part('work_header');
}
    
    ?>
 
</header>
