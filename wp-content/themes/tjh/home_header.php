  <div class="logo">
    <div class="the-logo">
        <a href="<?php echo get_permalink(5); ?>" class="nav-logo"><img src="<?php bloginfo('template_url'); ?>/images/logo.svg"></a>
    </div>
  </div>
  
  <div class="header-text-wrap">
   <div class="about">
    <h1>Tim Hamilton</h1>
    <?php the_field('description'); ?>
  </div>
  
  <div class="info">
  <div class="stats">
    <div class="status">
      <h2>Status</h2>
      <?php the_field('status'); ?>
    </div>
    <div class="contact">
      <h2>Contact</h2>
      <a href="mailto:tim@thegreathambino.co">tim@thegreathambino.co</a><br>
      (403) 615-4973
      <div class="social">
          <a href="https://twitter.com/TimHamiltonYYC" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
          <a href="https://dribbble.com/timhamilton6" target="_blank"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
      </div>
    </div>
  </div><!--stats-->


</div>
</div>