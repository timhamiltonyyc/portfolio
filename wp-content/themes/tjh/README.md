# This is a starter theme for my WordPress projects. Trying to work smarter, not harder by leveraging existing technologies to speed up front-end dev time. #

## FEATURES: ##
* Uses Flexbox for initial layout. [According to Morten Rand Hendriksen](https://www.linkedin.com/pulse/flexbox-ready-production-morten-rand-hendriksen), Flexbox is ready for production as long as we're supporting ie10 and above. I've found this is REALLY worth learning the basics of because it's been amazing at speeding up my dev time and reducing my pixel-tinkering time.
* Has a fully-built in, fully-mobile navigation in the style that we normally use on our sites (top nav, logo to the left, nav to the right). Just add your pages with WP-menu. You might have to tweak the
pixel width at which the mobile view kicks in, depending on the length of your nav.
* Uses a simplified SASS setup with mixins/variables for resets, transitions, fonts, and colors.
* Set up to use GRUNT for SASS preprocessing and auto vendor prefixing: run *npm install*, then run *npm install grunt-contrib-sass --save-dev*, then run *npm install grunt-contrib-watch --save-dev*, then run *npm install grunt-autoprefixer --save-dev*. then run *grunt*.
* functions.php enqueues Google Fonts for easy integration.
* Footer already contains copyright and credit info.
* Included templates are a stripped-down front page and a page.php and a single.php.
* A folder to hold sql backups if you want to save those too.

## TO-DO: ##
* Grunt: make a task called "build" for all final build steps like image compression, combining JS files, compressing CSS, etc.
* Use jQuery to analyze when a nav becomes too long, and switch to mobile view then.
* Add submenu support for the nav.
