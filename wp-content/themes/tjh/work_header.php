  <div class="logo">
    <div class="the-logo">
        <a href="<?php echo get_permalink(5); ?>" class="nav-logo"><img src="<?php bloginfo('template_url'); ?>/images/logo.svg"></a>
    </div>
  </div>
  
<div class="work-header header-text-wrap">
<h1><?php the_title(); ?></h1>

<?php the_field('project_description'); ?>
</div>