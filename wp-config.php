<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tjh');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         't:2<UgO5187 TO9Z$%7jo|I;yg($Q]k4X5<A8tE2p!;a3P3:Lq~o0#zX,(6JnQe}');
define('SECURE_AUTH_KEY',  'y`;6U?S}=~PGS*@V{I1 r^r+:Y_Cs]x?j_R-]!MW!uh<?o`uXLsDZ,dBFcp~,p|q');
define('LOGGED_IN_KEY',    'PZCpDx-Q~~cz <XZj/TYZAZ3mxyOwi#8}6)0B8hnWdN Dm<x)1UD2L(`7#`}v(ox');
define('NONCE_KEY',        '~XBea:Njc0&AhV3@:sKe{i5(hO:fT2%% iEtHHi8v>ka9`qU)uzcas>|uV`f|+Aa');
define('AUTH_SALT',        'a=s2.%X,m+>8Q(~zaF%)Q0lvUQ[y&hGNt3m45fFz^)a1(#hl9~ ge`OFBcFjRU#~');
define('SECURE_AUTH_SALT', '+X&TU!Z{1Vgnubh#^q]9j.Ag*1zh!BG,ev>e`<ZYOI(W=:xBaEK{y^R3+Q #0Ub[');
define('LOGGED_IN_SALT',   '/x>[huwYlK|(lv!bwGRPI<?s]~Y+RslVv_G 9{36mw%lnS/IH%R2>{?h`Ki]ih_O');
define('NONCE_SALT',       '(?&3:6M{5S;vnr=]koXN&s.P2<G_{x`Jr.b&S<kNK7G+uB6|{?lFrOo=qR{G9Lhp');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tjh_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
